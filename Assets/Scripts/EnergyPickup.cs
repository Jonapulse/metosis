﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPickup : MonoBehaviour
{
    public LayerMask playerLayer;
    ContactFilter2D playerFilter;
    private Collider2D coll;

    void Start()
    {
        playerFilter = new ContactFilter2D();
        playerFilter.SetLayerMask(playerLayer);
        coll = GetComponent<Collider2D>();
    }

    void Update()
    {
        List<Collider2D> hitList = new List<Collider2D>();
        if(coll.OverlapCollider(playerFilter, hitList) > 0)
        {
            hitList[0].gameObject.GetComponent<PlayerController>().mitosisEnergy += 2;
            SoundController.Instance.PlayPickup();
            Destroy(this.gameObject);
        }
    }
}

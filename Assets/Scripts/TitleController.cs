﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class TitleController : MonoBehaviour
{
    [SerializeField] GameObject cursor;
    [SerializeField] GameObject lvlSelectLeft;
    [SerializeField] GameObject lvlSelectRight;
    [SerializeField] TextMeshProUGUI newGameText;
    [SerializeField] TextMeshProUGUI continueText;

    private Color disabledCor = Color.Lerp(Color.red, Color.black, 0.5f);

    int selectedInd = 0;
    int selectedLevel = 0;
    int maxUnlockedLevel;
    float timer = 0;
    const float INPUT_DELAY = 0.25f;

    void Start()
    {
        maxUnlockedLevel = PlayerPrefs.GetInt("maxUnlockedLevel", 0);
        if (maxUnlockedLevel == 0)
        {
            SetButton(0);
            continueText.color = disabledCor;
        }
        else
        {
            SetButton(1);
            SetLevelSelect(maxUnlockedLevel);
        }

    }

    void Update()
    {   
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if (timer > 0)
            timer -= Time.deltaTime;

        if (Mathf.Abs(input.x) > 0.5f && timer <= 0 && selectedInd == 1)
        {
            timer = INPUT_DELAY;
            SoundController.Instance.PlaySingleTypewriterClick();
            if (input.x < 0 && selectedLevel > 1)
                SetLevelSelect(selectedLevel - 1);
            else if (input.x > 0 && selectedLevel < maxUnlockedLevel)
                SetLevelSelect(selectedLevel + 1);
        }
        else if (Mathf.Abs(input.y) > 0.5f && timer <= 0 && maxUnlockedLevel > 0)
        {
            timer = INPUT_DELAY;
            SoundController.Instance.PlaySingleTypewriterClick();
            SetButton((selectedInd + 1) % 2);
        }

        if (Input.GetButtonDown("Submit") && timer <= 0)
        {
            SoundController.Instance.PlaySingleTypewriterClick();
            timer = INPUT_DELAY;
            if(selectedInd == 0)
                MetosisController.Instance.TitleToGame(1);
            else
                MetosisController.Instance.TitleToGame(selectedLevel);
        }

    }

    void SetButton(int newInd)
    {
        selectedInd = newInd;

        if (selectedInd == 0)
            cursor.transform.position = new Vector3(cursor.transform.position.x, newGameText.transform.position.y - 100, cursor.transform.position.z);
        else
            cursor.transform.position = new Vector3(cursor.transform.position.x, continueText.transform.position.y - 100, cursor.transform.position.z);
    }

    void SetLevelSelect(int level)
    {
        selectedLevel = level;

        if (level == maxUnlockedLevel)
            continueText.text = "Continue";
        else
            continueText.text = "Level " + level;

        lvlSelectLeft.SetActive(selectedLevel > 1);
        lvlSelectRight.SetActive(selectedLevel < maxUnlockedLevel);
    }

    public void NewGameClick()
    {
        SoundController.Instance.PlaySingleTypewriterClick();
        MetosisController.Instance.TitleToGame(1);
    }

    public void ContinueClick()
    {
        SoundController.Instance.PlaySingleTypewriterClick();
        MetosisController.Instance.TitleToGame(selectedLevel);
    }

    public void ChangeLevel(int change)
    {
        SoundController.Instance.PlaySingleTypewriterClick();
        if (selectedLevel + change >= 0 && selectedLevel + change <= maxUnlockedLevel)
            SetLevelSelect(selectedLevel + change);
    }
}

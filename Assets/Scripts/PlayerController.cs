﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //2DPlatformer
    public float jumpHeight = 1;
    public float timeToJumpApex = .35f;
    float accelerationTimeAirborne = 0.05f;
    float accelerationTimeGrounded = 0.05f;
    public float moveSpeed = 3;
    public float bounceHeight = 5;

    float gravity;
    float jumpVelocity;
    Vector3 velocity;
    float velocityXSmoothing;

    Controller2D controller;
    //End 2DPlatformer

    [SerializeField] List<GameObject> holdSearchPoints;
    [SerializeField] LayerMask deathLayer;
    [SerializeField] LayerMask exitLayer;
    [SerializeField] LayerMask bounceLayer;
    [SerializeField] LayerMask floorLayer;

    private bool canJump = true;
    private float jumpCooldown = 0.3f;
    private bool canSpawn = true;
    private float spawnCooldown = 0.45f;
    [SerializeField] public SpriteRenderer[] flipUs;
    private Animator anim;
    [SerializeField] public GameObject[] mitosisIndicator;
    [SerializeField] public GameObject reverseIndicator;
    [SerializeField] public GameObject mitosisRefundPrefab;

    private BoxCollider2D coll;
    private ContactFilter2D deathFilter;
    private ContactFilter2D bounceFilter;
    private ContactFilter2D exitFilter;

    public int mitosisEnergy = 2; //Can clone if >1 energy
    int energyEnRoute = 0; //Used to choose which clone to send new energy to
    public int mirrorMove = 1; //*-1 every clone
    [SerializeField] float cloneSeperation = 0.75f;

    bool faceRight = true;
    bool wasGrounded = false;
    public bool dead = false;

    PlayerController partner = null;

    public string cloneID = "A";
    ContactFilter2D playerFilter;
    LayerMask originalCollisionMask;
    int originalLayer;
    PlayerController holder = null;
    PlayerController holdee = null;
    bool waitingToRegrab = false;

    bool lockWalkingAnim = false;

    private int overrideThrow = 0; //Determines velocity when thrown
    bool facedOppositeAsBaseWhenThrown;
    bool needToLandFromThrow = false;

    private void Start()
    {
        reverseIndicator.SetActive(mirrorMove < 0);
        anim = GetComponent<Animator>();
        coll = GetComponent<BoxCollider2D>();

        deathFilter = new ContactFilter2D();
        deathFilter.SetLayerMask(deathLayer);
        deathFilter.useTriggers = true;
        bounceFilter = new ContactFilter2D();
        bounceFilter.SetLayerMask(bounceLayer);
        bounceFilter.useTriggers = true;
        exitFilter = new ContactFilter2D();
        exitFilter.SetLayerMask(exitLayer);
        exitFilter.useTriggers = true;

        anim.SetInteger("mirrorMove", mirrorMove);

        //2D Platformer
        controller = GetComponent<Controller2D>();
        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;       

        SetupClone(cloneID);
    }

    void FixedUpdate()
    {
        if (dead)
            return;

        if (!MetosisController.Instance.allowControl)
        {
            velocity.x = 0;
            if(!lockWalkingAnim)
                anim.SetFloat("AbsHorSpeed", Mathf.Abs(velocity.x));

            velocity.y += gravity * Time.deltaTime;
            if (controller.collisions.above || controller.collisions.below)
            {
                velocity.y = 0;
                anim.SetBool("grounded", true);
            }
            List<Collider2D> hitListJustForL15 = new List<Collider2D>();
            if (coll.OverlapCollider(deathFilter, hitListJustForL15) > 0)
            {
                Die();
                return;
            }

            controller.Move(velocity * Time.deltaTime, 1);
            return;
        }

        //Collisions
        List<Collider2D> hitList = new List<Collider2D>();
        if (coll.OverlapCollider(deathFilter, hitList) > 0)
        {
            Die();
            return;
        }

        if (coll.OverlapCollider(exitFilter, hitList) > 0)
        {
            MetosisController.Instance.ExitReached();
            MoveRight();
            return;
        }
        if (coll.OverlapCollider(bounceFilter, hitList) > 0 && holder == null)
        {
            if (velocity.y < -0.5f && (transform.position.y - hitList[0].transform.position.y) > 0.4f)
            {
                velocity.y = bounceHeight;
                hitList[0].GetComponent<Animator>().SetTrigger("bounce");
                SoundController.Instance.PlayHiccup();
            }
        }

        if (!waitingToRegrab)
        {
            foreach(GameObject holdCheck in holdSearchPoints)
            {
                if (Physics2D.OverlapPoint(holdCheck.transform.position, playerFilter, hitList) > 0)
                {
                    PlayerController checkHolder = hitList[0].gameObject.GetComponent<PlayerController>();

                    //If being held, or not high enough, or grabbing would force into wall then skip
                    if (checkHolder.holdee != null || (transform.position.y - checkHolder.transform.position.y) < 1.1f || Physics2D.OverlapBox((Vector2)checkHolder.transform.position + Vector2.up * 0.9f, new Vector2(0.25f, 1.2f), 0, floorLayer))
                        continue;

                    checkHolder.SetHoldee(this);
                    SetHolder(checkHolder);
                    waitingToRegrab = true;
                    SoundController.Instance.PlayGrab();
                    break;
                }
            }
        }

        //Raycast Physics
        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }

        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (holder != null)
        {
            input.x = 0;
            transform.position = new Vector3(holder.transform.position.x, holder.transform.position.y + 1.2f, transform.position.z);
            controller.collisions.below = true;
        }

        if (Input.GetButton("Jump") && controller.collisions.below && canJump)
        {
            if(holdee == null)
            {
                velocity.y = jumpVelocity;
                SoundController.Instance.PlayJump();
                if (holder)
                {
                    int throwVelocity = 0;
                    if (GetBase().velocity.x > 0.5f)
                        throwVelocity = 1;
                    if (GetBase().velocity.x < -0.5f)
                        throwVelocity = -1;
                    StartCoroutine(OverrideMirrorMoveUntilGrounded(throwVelocity, GetBase().mirrorMove != mirrorMove));
                    WaitForRegrab();
                    holder.SetHoldee(null);
                    SetHolder(null);
                }
            }
            StartCoroutine(DoJumpCooldown());
        }

        float targetVelocityX = !needToLandFromThrow ? input.x * mirrorMove * moveSpeed : overrideThrow * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
        if (holder == null)
        {
            controller.Move(velocity * Time.deltaTime, GetStackHeightAboveMe());
        }

        //Animation
        anim.SetFloat("AbsHorSpeed", Mathf.Abs(velocity.x));
        anim.SetBool("grounded", controller.collisions.below || wasGrounded);
        wasGrounded = controller.collisions.below; //Gives one frame of leniency for the animation to protect against a one frame grounded error when passing the top of stairs

        if (holder == null)
        {
            if (velocity.x < -0.1f)
            {
                faceRight = false;
            }
            else if (velocity.x > 0.1f)
            {
                faceRight = true;
            }

            //override if momentum throw
            if (needToLandFromThrow)
            {
                faceRight = facedOppositeAsBaseWhenThrown ? overrideThrow < 0 : overrideThrow > 0;
            }
        }
        else
        {
            PlayerController bottom = GetBase();
            faceRight = bottom.mirrorMove == mirrorMove ? bottom.faceRight : !bottom.faceRight;
        }

        foreach (SpriteRenderer spr in flipUs)
            spr.flipX = !faceRight;

        anim.SetBool("holding", holdee != null);

        //Clone stuff
        if (partner != null && partner.dead)
        {
            partner = null;
        }

        for(int i = 0; i < mitosisIndicator.Length; i++)
            mitosisIndicator[i].SetActive(i + 1 <= mitosisEnergy/2);
        
        if (MetosisController.Instance.allowControl && Input.GetButton("Split"))
        {
            if(holder == null && holdee == null)
                AttemptSplit();
            else if(this == GetBase())
            {
                //Attempt Split from the top down
                List<PlayerController> stackFromBot = new List<PlayerController>();
                PlayerController searchPlayer = this;
                while(searchPlayer != null)
                {
                    stackFromBot.Add(searchPlayer);
                    searchPlayer = searchPlayer.holdee;
                }

                for (int i = stackFromBot.Count - 1; i >= 0; i--)
                    stackFromBot[i].AttemptSplit();
            }
        }
    }

    public void AttemptSplit()
    {
        if(mitosisEnergy/2 > 0 && canSpawn)
        {
            DoSpawnCooldown();

            //Check bounds
            bool spawning = false;
            Vector3 origSpawnVec = Vector3.zero;
            Vector3 newSpawnVec = Vector3.zero;

            Vector2 checkSize = new Vector2(coll.size.x - Controller2D.skinWidth * 2, coll.size.y - Controller2D.skinWidth * 2);

            RaycastHit2D rightHit = Physics2D.BoxCast((Vector2)transform.position + coll.offset, checkSize, 0, Vector2.right, cloneSeperation / 2, controller.collisionMask);
            RaycastHit2D leftHit = Physics2D.BoxCast((Vector2)transform.position + coll.offset, checkSize, 0, Vector2.left, cloneSeperation / 2, controller.collisionMask);

            bool faceRightPlusStackInfo = faceRight;
            if (holder != null)
                faceRightPlusStackInfo = GetBase().faceRight;

            if (rightHit.collider != null) //Hit right side
            {
                leftHit = Physics2D.BoxCast(rightHit.centroid + Vector2.left * 0.1f, checkSize, 0, Vector2.left, cloneSeperation, controller.collisionMask);
                if (leftHit.collider == null)
                {
                    origSpawnVec = faceRightPlusStackInfo ? rightHit.centroid : rightHit.centroid + Vector2.left * cloneSeperation;
                    newSpawnVec = faceRightPlusStackInfo ? rightHit.centroid + Vector2.left * cloneSeperation : rightHit.centroid;
                    spawning = true;
                }
            }
            else if (leftHit.collider != null) //Hit left side
            {
                rightHit = Physics2D.BoxCast(leftHit.centroid + Vector2.right * 0.1f, checkSize, 0, Vector2.right, cloneSeperation, controller.collisionMask);
                if (rightHit.collider == null)
                {
                    origSpawnVec = !faceRightPlusStackInfo ? leftHit.centroid : leftHit.centroid + Vector2.right * cloneSeperation;
                    newSpawnVec = !faceRightPlusStackInfo ? leftHit.centroid + Vector2.right * cloneSeperation : leftHit.centroid;
                    spawning = true;
                }
            }
            else //No hit on spawn. Nice.
            {
                origSpawnVec = this.transform.position + (!faceRightPlusStackInfo ? Vector3.left : Vector3.right) * cloneSeperation / 2;
                newSpawnVec = this.transform.position + (!faceRightPlusStackInfo ? Vector3.right : Vector3.left) * cloneSeperation / 2;
                spawning = true;
            }

            if (spawning)
            {
                if (holder)
                {
                    holder.SetHoldee(null);
                    SetHolder(null);
                    waitingToRegrab = false;
                }
                if (holdee)
                {
                    holdee.SetHolder(null);
                    SetHoldee(null);
                    waitingToRegrab = false;
                }

                transform.position = origSpawnVec;

                GameObject clone = GameObject.Instantiate(this.gameObject, newSpawnVec, Quaternion.identity, this.transform.parent);

                PlayerController cloneContr = clone.GetComponent<PlayerController>();
                cloneContr.SetMirrorMove(this.mirrorMove * -1);

                int donatedEnergy = this.mitosisEnergy / 2;
                this.mitosisEnergy -= donatedEnergy;
                cloneContr.mitosisEnergy = donatedEnergy;
                cloneContr.faceRight = !this.faceRight;
                foreach (SpriteRenderer sprClone in cloneContr.flipUs)
                    sprClone.flipX = faceRight;

                cloneContr.SetVelocity(new Vector3(-velocity.x, velocity.y));

                cloneContr.cloneID = MetosisController.Instance.RegisterClone();
                cloneContr.partner = this;
                cloneContr.DoSpawnCooldown();
                this.partner = cloneContr;
                clone.GetComponent<Animator>().SetTrigger("MitosisEnd");
            }

            StartCoroutine(DelayedSpawnAction(spawning));
            anim.SetTrigger("MitosisStart");

            SoundController.Instance.PlaySpawn();
        }
    }

    public void SetMirrorMove(int mirrorToSet)
    {
        mirrorMove = mirrorToSet;
    }

    public void SetHoldee(PlayerController holdMe)
    {
        holdee = holdMe;
    }

    //When iveGotYou is null, the class treats itself as the baseHolder
    public void SetHolder(PlayerController iveGotYou)
    {
        holder = iveGotYou;
        PlayerController baseHolder = GetBase();

        if(baseHolder == this)
        {
            gameObject.layer = originalLayer;
            controller.collisionMask = originalCollisionMask;
        }

        PlayerController holdeeForUpdateLoop = this;
        while(holdeeForUpdateLoop != null)
        {
            holdeeForUpdateLoop.gameObject.layer = baseHolder.gameObject.layer;
            holdeeForUpdateLoop.controller.collisionMask = baseHolder.controller.collisionMask;
            holdeeForUpdateLoop = holdeeForUpdateLoop.holdee;
        }
    }

    IEnumerator OverrideMirrorMoveUntilGrounded(int throwDirection, bool oppositeFacing)
    {
        needToLandFromThrow = true;
        overrideThrow = throwDirection;
        facedOppositeAsBaseWhenThrown = oppositeFacing;

        yield return new WaitForSeconds(0.1f);

        while(!controller.collisions.below && !(coll.OverlapCollider(bounceFilter, new List<Collider2D>()) > 0))
        {
            yield return null;
        }

        needToLandFromThrow = false;
        overrideThrow = 0;
    }

    private PlayerController GetBase()
    {
        PlayerController baseHolder = this;
        while (baseHolder.holder != null)
            baseHolder = baseHolder.holder;

        return baseHolder;
    }

    private int GetStackHeightAboveMe()
    {
        PlayerController holderForSearch = GetBase();
        int height = 0;
        while (holderForSearch != null)
        {
            height++;
            holderForSearch = holderForSearch.holdee;
        }

        return height;
    }

    public void DoSpawnCooldown()
    {
        StartCoroutine(SpawnCooldownCoroutine());
    }

    IEnumerator SpawnCooldownCoroutine()
    {
        canSpawn = false;
        yield return new WaitForSeconds(spawnCooldown);
        canSpawn = true;
    }

    IEnumerator DelayedSpawnAction(bool spawning)
    {
        yield return new WaitForSeconds(0.45f);
        if (!spawning)
            SoundController.Instance.PlayFailedSpawn();
    }

    void WaitForRegrab()
    {
        StartCoroutine(WaitRegrabCoroutine());
    }

    IEnumerator WaitRegrabCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        waitingToRegrab = false;
    }

    public void SetupClone(string newID)
    {
        cloneID = newID;
        gameObject.layer = LayerMask.NameToLayer("player" + newID);
        originalLayer = gameObject.layer;

        //init player collisions
        LayerMask playerCollision = (1 << LayerMask.NameToLayer("playerA")) | (1 << LayerMask.NameToLayer("playerB")) | (1 << LayerMask.NameToLayer("playerC")) | (1 << LayerMask.NameToLayer("playerD"));
        controller.collisionMask = controller.collisionMask | playerCollision;
        playerFilter = new ContactFilter2D();
        playerFilter.SetLayerMask(playerCollision);

        //then remove self-collision
        controller.collisionMask = controller.collisionMask & ~(1 << gameObject.layer);
        originalCollisionMask = controller.collisionMask;
    }

    IEnumerator DoJumpCooldown()
    {
        canJump = false;
        yield return new WaitForSeconds(jumpCooldown);
        canJump = true;
    }

    public void SetVelocity(Vector3 newVelocity)
    {
        velocity = newVelocity;
    }

    private void Die(bool playSplat = true)
    {
        Debug.Log("But why?");
        dead = true;

        if (holder)
            holder.SetHoldee(null);
        if (holdee)
            holdee.SetHolder(null);

        foreach (GameObject mitInd in mitosisIndicator)
            mitInd.SetActive(false);
        reverseIndicator.SetActive(false);

        gameObject.layer = LayerMask.NameToLayer("deadplayer");
        MetosisController.Instance.RegisterPlayerDeath(cloneID);

        while(mitosisEnergy > 0)
        {
            PlayerController returnToMe = partner;
            if (returnToMe == null)
            {
                returnToMe = MetosisController.Instance.GetLivingPlayer();
            }

            if (returnToMe)
            {
                GameObject refund = GameObject.Instantiate(mitosisRefundPrefab, transform.position, Quaternion.identity);
                refund.GetComponent<TweenThenDestroy>().TweenToPlayer(returnToMe);
                returnToMe.RegisterEnergySending();
            }
            mitosisEnergy--;
        }
        

        StartCoroutine(AnimateDeath(playSplat));
    }

    public int GetMitosisEnergy()
    {
        return mitosisEnergy + energyEnRoute;
    }

    public void TriggerDeathForRestart()
    {
        Die(false);
    }
    IEnumerator AnimateDeath(bool playSplat)
    {
        anim.SetTrigger("Die");
        if(playSplat)
            SoundController.Instance.PlayGunHit();
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }

    //Story scripting
    public void StoryTurn(bool left)
    {
        faceRight = !left;

        foreach (SpriteRenderer spr in flipUs)
            spr.flipX = !faceRight;
    }

    public void MoveRight()
    {
        faceRight = true;
        foreach (SpriteRenderer spr in flipUs)
            spr.flipX = !faceRight;

        StartCoroutine(WalkRightFor(5f));
    }

    IEnumerator WalkRightFor(float seconds)
    {
        float timer = 0;
        lockWalkingAnim = true;
        while (timer < seconds)
        {
            timer += Time.deltaTime;

            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);

            yield return new WaitForFixedUpdate();
        }
        lockWalkingAnim = false;
    }

    public Vector3 GetMitosisIndicatorPosition()
    {
        return mitosisIndicator[0].transform.position;
    }

    public void RegisterEnergySending()
    {
        energyEnRoute++;
    }

    //For redirecting energy
    public void CancelEnergySending()
    {
        energyEnRoute--;
    }

    public void GrantEnergy()
    {
        energyEnRoute--;
        mitosisEnergy++;
    }
}

internal class Ienumerator
{
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStagger : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(DelayAnimator(GetComponent<Animator>()));
    }

    IEnumerator DelayAnimator(Animator anim)
    {
        float animSpeed = anim.speed;
        anim.speed = 0;
        yield return new WaitForSeconds(Random.Range(0, 0.25f));
        anim.speed = animSpeed;
    }

}

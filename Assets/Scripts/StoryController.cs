﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class StoryController : MonoBehaviour
{
    Collider2D coll;
   // Animator anim;
    SpriteRenderer lukeSprite;
    public bool triggerred = false;
    ContactFilter2D playerFilter;

    [SerializeField] Vector3 cameraZoomPos;
    [SerializeField] LayerMask playerLayer;
    [SerializeField] Line[] sceneLines;
    [SerializeField] GameObject emilyBox;
    [SerializeField] TextMeshProUGUI emilyText;
    [SerializeField] GameObject lukeBox;
    [SerializeField] TextMeshProUGUI lukeText;
    [SerializeField] bool isGenericStory = false;

    //Scene-specific links and vars
    [SerializeField] DoorController dropDoor;
    [SerializeField] SpriteRenderer escortWBC;

    [System.Serializable]
    public class Line
    {
        public bool emilySaysIt;
        public string line;
        public string action = "";
    }

    void Start()
    {
        coll = GetComponent<Collider2D>();
        playerFilter = new ContactFilter2D();
        playerFilter.SetLayerMask(playerLayer);
        //  anim = GetComponent<Animator>();
        lukeSprite = GetComponent<SpriteRenderer>();
        if(!isGenericStory)
            lukeSprite.color = Color.black;

        if(MetosisController.Instance.WasDialogueTriggered())
        {
            triggerred = true;
            CheckForSkip();
        }
    }

    void CheckForSkip()
    {
        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case (8):
                lukeSprite.color = Color.clear;
                escortWBC.color = Color.clear;
                dropDoor.enabled = true;
                break;
        }
        
    }

    void Update()
    {
        if (triggerred || !MetosisController.Instance.allowControl)
            return;

        List<Collider2D> hitList = new List<Collider2D>();
        if(coll.OverlapCollider(playerFilter, hitList) > 0)
        {
            MetosisController.Instance.SetDialogueTriggered(SceneManager.GetActiveScene().buildIndex);
            StartCoroutine(DoCutscene(hitList[0].gameObject));
        }
    }

    public IEnumerator DoCutscene(GameObject emilyObj, bool resetMusicAtEnd = true)
    {
        triggerred = true;
        PlayerPrefs.SetString(SceneManager.GetActiveScene().buildIndex.ToString(), "triggered");

        if (!isGenericStory)
            lukeSprite.DOColor(Color.white, 2f);
        float zoomTime = 2f;
        Camera.main.transform.DOMove(cameraZoomPos, zoomTime).SetEase(Ease.InOutQuad);
        Camera.main.DOOrthoSize(1.5f, zoomTime).SetEase(Ease.InOutQuad);
        MetosisController.Instance.allowControl = false;
        SoundController.Instance.ToCutsceneMusic();

        yield return new WaitForSeconds(zoomTime);

        //Put scene-specific vars here
        //
        bool hideLuke = false;
        bool delayReturningControl = false;
        ///////////////////////

        int sceneProgress = 0;

        emilyBox.transform.position = Camera.main.WorldToScreenPoint(emilyObj.transform.position) + Vector3.up * 380f;
        if(!isGenericStory)
            lukeBox.transform.position = Camera.main.WorldToScreenPoint(transform.position) + Vector3.up * 380f;

        while (sceneProgress < sceneLines.Length)
        {
            Line thisLine = sceneLines[sceneProgress];

            if (thisLine.action.Equals(""))
            {
                emilyBox.SetActive(thisLine.emilySaysIt);
                lukeBox.SetActive(!thisLine.emilySaysIt);

                TextMeshProUGUI speakingText = emilyText;
                if (!thisLine.emilySaysIt)
                    speakingText = lukeText;


                int lineProgress = 0;
                string fullLine = thisLine.line;

                SoundController.Instance.StartTypewriter();
                while (lineProgress < fullLine.Length)
                {
                    lineProgress++;
                    if (Input.anyKeyDown)
                        lineProgress = fullLine.Length;

                    speakingText.text = fullLine.Substring(0, lineProgress);

                    yield return new WaitForSeconds(0.01f);
                }
                SoundController.Instance.StopTypewriter();

                yield return new WaitForEndOfFrame(); //So we don't skip through all

                while (!Input.anyKeyDown)
                    yield return null;
            }
            else
            {
                emilyBox.SetActive(false);
                lukeBox.SetActive(false);
                switch(thisLine.action)
                {
                    case ("turnaround"):
                        yield return new WaitForSeconds(0.5f);
                        emilyObj.GetComponent<PlayerController>().StoryTurn(true);
                        yield return new WaitForSeconds(0.5f);
                        break;
                    case ("turnaroundright"):
                        yield return new WaitForSeconds(0.5f);
                        emilyObj.GetComponent<PlayerController>().StoryTurn(false);
                        yield return new WaitForSeconds(0.5f);
                        break;
                    case ("leave"):
                        yield return new WaitForSeconds(0.5f);
                        emilyObj.GetComponent<PlayerController>().MoveRight();
                        yield return new WaitForSeconds(3f);
                        break;
                    case ("luketurnaround"):
                        yield return new WaitForSeconds(0.5f);
                        lukeSprite.flipX = true;
                        yield return new WaitForSeconds(0.5f);
                        break;
                    case ("betrayalfade"):
                        lukeSprite.DOFade(0, 1f);
                        hideLuke = true;
                        delayReturningControl = true;
                        yield return new WaitForSeconds(0.5f);
                        escortWBC.flipX = false;
                        yield return new WaitForSeconds(0.25f);
                        escortWBC.DOFade(0, 1f);
                        yield return new WaitForSeconds(0.25f);
                        StartCoroutine(OpenTrapDoor(0.5f + zoomTime));
                        break;
                    case ("exit"):
                        yield return new WaitForSeconds(1f);
                        MetosisController.Instance.ExitReached();
                        yield break;
                }
            }
            

            sceneProgress++;
            yield return null;
        }

        emilyBox.SetActive(false);
        lukeBox.SetActive(false);
        if(resetMusicAtEnd)
            SoundController.Instance.ToGameplayMusic();

        yield return new WaitForSeconds(0.5f);

        if (!isGenericStory && !hideLuke)
            lukeSprite.DOColor(Color.black, 2f);
        Camera.main.transform.DOMove(new Vector3(0, 0, -10f), zoomTime).SetEase(Ease.InOutQuad);
        Camera.main.DOOrthoSize(5f, zoomTime).SetEase(Ease.InOutQuad);
        if (!delayReturningControl)
            MetosisController.Instance.allowControl = true;
        else
            StartCoroutine(ReturnControl(zoomTime));
    }

    IEnumerator OpenTrapDoor(float delay)
    {
        yield return new WaitForSeconds(delay);
        dropDoor.enabled = true;
    }

    IEnumerator ReturnControl(float delay)
    {
        yield return new WaitForSeconds(delay);
        MetosisController.Instance.allowControl = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteBloodCellController : MonoBehaviour
{
    [SerializeField] GameObject antibodyPrefab;
    [SerializeField] BoxCollider2D detectRect;
    [SerializeField] LayerMask playerLayer;
    [SerializeField] LayerMask impenetrableLayer;
    [SerializeField] GameObject bulletSpawn;

    ContactFilter2D playerFilter;
    ContactFilter2D floorFilter;
    SpriteRenderer playerSprite;
    Animator anim;

    float fireTimer = 0;
    float fireCooldown = 3f;
    float fireVelocity = 7.5f;
    float localStartBulletX;
    float fireDelay = 0.35f;

    private void Start()
    {
        playerFilter = new ContactFilter2D();
        playerFilter.SetLayerMask(playerLayer);
        floorFilter = new ContactFilter2D();
        floorFilter.SetLayerMask(impenetrableLayer);
        playerSprite = GetComponent<SpriteRenderer>();
        localStartBulletX = bulletSpawn.transform.localPosition.x;
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        List<Collider2D> playersInSight = new List<Collider2D>();
        if (detectRect.OverlapCollider(playerFilter, playersInSight) > 0)
        {
            Collider2D closestPlayer = playersInSight[0];
            foreach (Collider2D player in playersInSight)
                if ((transform.position - closestPlayer.transform.position).magnitude > (transform.position - player.transform.position).magnitude)
                    closestPlayer = player;

            float fireDir = 1;
            if (closestPlayer.transform.position.x < transform.position.x) {
                fireDir = -1;
                playerSprite.flipX = true;
                bulletSpawn.transform.localPosition = new Vector3(-localStartBulletX, bulletSpawn.transform.localPosition.y, bulletSpawn.transform.localPosition.z); 
            }
            else
            {
                playerSprite.flipX = false;
                bulletSpawn.transform.localPosition = new Vector3(localStartBulletX, bulletSpawn.transform.localPosition.y, bulletSpawn.transform.localPosition.z);
            }

            if(fireTimer <= 0)
                StartCoroutine(FireAntibody(fireDir));

        }

        if (fireTimer > 0)
            fireTimer -= Time.deltaTime;
    }
    IEnumerator FireAntibody(float fireDir)
    {
        anim.SetTrigger("Fire");
        fireTimer = fireCooldown;

        yield return new WaitForSeconds(fireDelay);

        SoundController.Instance.PlayGunShoot();
        Rigidbody2D antibody = (GameObject.Instantiate(antibodyPrefab, bulletSpawn.transform.position, Quaternion.identity)).GetComponent<Rigidbody2D>();
        antibody.velocity = Vector3.right * fireVelocity * fireDir;
        antibody.transform.localScale = new Vector3(fireDir, 1, 1);
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SoundController : MonoBehaviour
{
    //SINGLETON STUFF
    protected SoundController() { }
    private static SoundController _instance = null;

    public static SoundController Instance
    {
        get
        {
            return SoundController._instance;
        }
    }

    [SerializeField] AudioSource sfxSource;
    [SerializeField] AudioSource musicCutsceneSource;
    [SerializeField] AudioSource musicGameplaySource;
    [SerializeField] AudioSource rumbleSource;

    [SerializeField] AudioClip deathClip;
    [SerializeField] AudioClip gunHit;
    [SerializeField] AudioClip gunShoot;
    [SerializeField] AudioClip jumpClip;
    [SerializeField] AudioClip nueronOff;
    [SerializeField] AudioClip spawnSound;
    [SerializeField] AudioClip typewriter;
    [SerializeField] AudioClip failedSpawn;
    [SerializeField] AudioClip[] ricochets;
    [SerializeField] AudioClip[] hiccups;
    [SerializeField] AudioClip pickup;
    [SerializeField] AudioClip grab;
    [SerializeField] AudioClip vomit;
    [SerializeField] AudioClip typewriterClick;

    bool sfxOn = true;
    bool musicOn = true;
    bool doingCutsceneMusic = false;

    private void Awake()
    {
        if (_instance != this && _instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;

        DontDestroyOnLoad(this.gameObject);

        musicCutsceneSource.Play();
        musicGameplaySource.Play();
    }

    public void ToggleSfx(bool on)
    {
        sfxOn = on;
        sfxSource.volume = on ? 1 : 0;
    }

    public void ToggleMusic(bool on)
    {
        musicOn = on;
        if(!musicOn)
        {
            musicCutsceneSource.volume = 0;
            musicGameplaySource.volume = 0;
        }
        else
        {
            musicCutsceneSource.volume = doingCutsceneMusic ? 1 : 0;
            musicGameplaySource.volume = doingCutsceneMusic ? 0 : 1;
        }
    }

    public void PlayDeathClip()
    {
        sfxSource.PlayOneShot(deathClip);
    }

    public void PlayGunHit()
    {
        sfxSource.PlayOneShot(gunHit);
    }

    public void PlayGunShoot()
    {
        sfxSource.PlayOneShot(gunShoot);
    }

    public void PlayJump()
    {
        sfxSource.PlayOneShot(jumpClip);
    }
    public void PlayNeuronOff()
    {
        sfxSource.PlayOneShot(nueronOff);
    }
    public void PlaySpawn()
    {
        sfxSource.PlayOneShot(spawnSound);
    }

    public void PlayFailedSpawn()
    {
        sfxSource.PlayOneShot(failedSpawn);
    }

    public void PlayHiccup()
    {
        sfxSource.PlayOneShot(hiccups[Random.Range(0, hiccups.Length)]);
    }

    public void PlayRicochet()
    {
        sfxSource.PlayOneShot(ricochets[Random.Range(0, ricochets.Length)]);
    }

    public void PlayPickup()
    {
        sfxSource.PlayOneShot(pickup);
    }

    public void PlayGrab()
    {
        sfxSource.PlayOneShot(grab);
    }

    public void ImGonnaDotDotDot()
    {
        sfxSource.PlayOneShot(vomit);
    }

    public void StartTypewriter()
    {
        sfxSource.PlayOneShot(typewriter);
    }

    public void StopTypewriter()
    {
        sfxSource.Stop();
    }

    public void PlaySingleTypewriterClick()
    {
        sfxSource.PlayOneShot(typewriterClick);
    }

    public void ToggleRumble(bool on)
    {
        rumbleSource.Play();
        rumbleSource.DOFade(on ? 1 : 0, 1f);
    }

    public void ToCutsceneMusic(bool rush = false)
    {
        doingCutsceneMusic = true;
        if (!musicOn)
            return;

        if (!rush)
        {
            musicGameplaySource.DOFade(0, 1f);
            musicCutsceneSource.DOFade(1, 1f);
        } 
        else
        {
            musicGameplaySource.volume = 0;
            musicCutsceneSource.volume = 1;
        }
    }

    public void ToGameplayMusic(bool rush = false)
    {
        doingCutsceneMusic = false;
        if (!musicOn)
            return;

        if (!rush)
        {
            musicGameplaySource.DOFade(1, 1f);
            musicCutsceneSource.DOFade(0, 1f);
        }
        else
        {
            musicGameplaySource.volume = 1;
            musicCutsceneSource.volume = 0;
        }
    }

    public void ToNoMusic()
    {
        musicGameplaySource.DOFade(0, 1f);
        musicCutsceneSource.DOFade(0, 1f);
    }

    public void StopMusic()
    {
        musicGameplaySource.Stop();
        musicCutsceneSource.Stop();
    }
}

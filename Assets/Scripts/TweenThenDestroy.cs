﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenThenDestroy : MonoBehaviour
{

    const float tweenTime = 1.5f;

    public void TweenToPlayer(PlayerController pCont)
    {
        StartCoroutine(DoPlayerTween(pCont));
    }

    IEnumerator DoPlayerTween(PlayerController pCont)
    {
        Vector3 origPosition = transform.position;
        float timer = 0;
        while(timer < tweenTime)
        {
            timer += Time.deltaTime;

            if (pCont.dead)
            {
                pCont = MetosisController.Instance.GetLivingPlayer(); //Attempt to find a new grantee
                if(pCont == null)
                {
                    Destroy(this.gameObject);
                    yield return 0;
                }
                else
                {
                    origPosition = transform.position;
                    pCont.RegisterEnergySending();
                }

                yield return 0;
            }
            else
            {
                PlayerController alternateLowEnergyCandidate = MetosisController.Instance.GetLivingPlayer();
                if(pCont.GetMitosisEnergy() > alternateLowEnergyCandidate.GetMitosisEnergy() + 1)
                {
                    pCont.CancelEnergySending();
                    pCont = alternateLowEnergyCandidate;
                    pCont.RegisterEnergySending();
                }
            }



            transform.position = Vector3.Lerp(origPosition, pCont.GetMitosisIndicatorPosition(), Mathf.Pow(timer / tweenTime, 3));

            yield return null;
        }

        pCont.GrantEnergy();
        Destroy(this.gameObject);
    }
}

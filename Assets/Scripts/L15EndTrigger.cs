﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class L15EndTrigger : MonoBehaviour
{
    [SerializeField] DoorController[] doorsForListening;
    [SerializeField] Transform[] WBCsToFall;
    [SerializeField] StoryController endStory;
    [SerializeField] GameObject acidBall;
    [SerializeField] SpriteRenderer storyEmily;
    [SerializeField] Transform acidToDoor;
    [SerializeField] Transform acidColumn;
    [SerializeField] Transform doorsForRise;
    [SerializeField] Transform trainSides;
    bool endTriggered = false;
    void Update()
    {
        if(!endTriggered)
        {
            bool allTriggered = true;
            foreach(DoorController door in doorsForListening)
            {
                if (!door.IsHacked())
                    allTriggered = false;
            }

            if (allTriggered)
                EndItYouCowardDotDotDotJustDoItDotDotDotWhatAreYouWaitingFor();

            //DEBUG
            if (Input.GetKey(KeyCode.Y) && Input.GetKey(KeyCode.O))
                EndItYouCowardDotDotDotJustDoItDotDotDotWhatAreYouWaitingFor();
        }
    }

    //Like in a scene where a villain has lost and is goading the hero into killing him but their tender hero heart won't let them. Because this could be read in other ways.
    void EndItYouCowardDotDotDotJustDoItDotDotDotWhatAreYouWaitingFor()
    {
        StartCoroutine(EndScene());
    }

    IEnumerator EndScene()
    {
        endTriggered = true;
        MetosisController.Instance.allowControl = false;
        SoundController.Instance.ToNoMusic();
        foreach (DoorController door in doorsForListening)
            door.GetComponent<Animator>().SetTrigger("open");


        //Drop the WBCs
        //
        Random.InitState(69);
        foreach (Transform wbc in WBCsToFall)
        {
            wbc.DOMoveY(-7, 2).SetEase(Ease.InQuad);
            yield return new WaitForSeconds(Random.Range(0, 0.05f));
        }

        yield return new WaitForSeconds(1.5f);

        float projectileTime = 1.5f;
        float delayBetweenSplashes = 0.125f;

        SoundController.Instance.ToggleRumble(true);
        SoundController.Instance.PlayGunHit();
        GameObject acidBall1 = GameObject.Instantiate(acidBall, new Vector3(0.3f, -4.26f, 0), Quaternion.identity);
        //TR em at 8.15, 2.15, arc to 2.82
        float peak1Time = 0.7f * projectileTime;
        acidBall1.transform.DOMoveX(8.15f, projectileTime).SetEase(Ease.Linear);
        acidBall1.transform.DOMoveY(2.82f, peak1Time).SetEase(Ease.OutQuad);
        acidBall1.transform.DOMoveY(2.15f, projectileTime - peak1Time).SetEase(Ease.InQuad).SetDelay(peak1Time);

        yield return new WaitForSeconds(delayBetweenSplashes);

        SoundController.Instance.PlayGunHit();
        GameObject acidBall2 = GameObject.Instantiate(acidBall, new Vector3(0.9f, -4.26f, 0), Quaternion.identity);
        //TL em at -8.15, 2.7, arc to 3.84
        float peak2Time = 0.7f * projectileTime;
        acidBall2.transform.DOMoveX(-8.15f, projectileTime).SetEase(Ease.Linear);
        acidBall2.transform.DOMoveY(3.84f, peak2Time).SetEase(Ease.OutQuad);
        acidBall2.transform.DOMoveY(2.7f, projectileTime - peak2Time).SetEase(Ease.InQuad).SetDelay(peak2Time);

        yield return new WaitForSeconds(delayBetweenSplashes);

        SoundController.Instance.PlayGunHit();
        GameObject acidBall3 = GameObject.Instantiate(acidBall, new Vector3(-0.3f, -4.26f, 0), Quaternion.identity);
        //BR em at 7.45, -3.9, arc to -2
        float peak3Time = 0.5f * projectileTime;
        acidBall3.transform.DOMoveX(7.45f, projectileTime).SetEase(Ease.Linear);
        acidBall3.transform.DOMoveY(-2f, peak3Time).SetEase(Ease.OutQuad);
        acidBall3.transform.DOMoveY(-3.9f, projectileTime - peak3Time).SetEase(Ease.InQuad).SetDelay(peak3Time);

        yield return new WaitForSeconds(projectileTime);
        acidBall1.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
        yield return new WaitForSeconds(delayBetweenSplashes);
        acidBall2.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
        yield return new WaitForSeconds(delayBetweenSplashes);
        acidBall3.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);

        //Perform the scene
        //
        PlayerController survivor = MetosisController.Instance.GetLivingPlayer();
        survivor.GetComponent<SpriteRenderer>().DOFade(0, 2f);
        foreach (SpriteRenderer child in survivor.GetComponentsInChildren<SpriteRenderer>())
            child.DOFade(0, 2f);
        storyEmily.DOFade(1, 2f);
        foreach (DoorController door in doorsForListening)
            door.GetComponent<Animator>().SetBool("open", false);
        DOVirtual.DelayedCall(3f, ToggleRumbleOff);

        yield return StartCoroutine(endStory.DoCutscene(storyEmily.gameObject, false));

        //Rocket away
        //
        MetosisController.Instance.allowControl = false;
        SoundController.Instance.ToggleRumble(true);

        yield return new WaitForSeconds(2f);

        SoundController.Instance.ToggleMusic(false);
        SoundController.Instance.ImGonnaDotDotDot();

        acidToDoor.DOMoveY(0.9f, 2f).SetEase(Ease.Linear);

        yield return new WaitForSeconds(2f);

        float riseTime = 5f;
        acidColumn.transform.position = new Vector3(acidColumn.transform.position.x, 3.9f, acidColumn.transform.position.z);
        acidColumn.DOMoveY(17.85f, riseTime).SetEase(Ease.InQuad);
        doorsForRise.DOMoveY(13.95f, riseTime).SetEase(Ease.InQuad);
        trainSides.DOMoveY(13.95f, riseTime).SetEase(Ease.InQuad);

        float fadeOutTime = 2f;
        yield return new WaitForSeconds(riseTime - fadeOutTime);
        GetComponent<SpriteRenderer>().DOFade(1, fadeOutTime);

        yield return new WaitForSeconds(fadeOutTime);

        SoundController.Instance.ToCutsceneMusic();
        MetosisController.Instance.ExitReached();
    }

    void ToggleRumbleOff()
    {
        SoundController.Instance.ToggleRumble(false);
    }
}

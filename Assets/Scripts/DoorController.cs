﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] GameObject[] neuronObjects;
    Neuron[] neurons;
    [SerializeField] LayerMask playerMask;

    Collider2D doorColl;
    ContactFilter2D playerFilter;
    Animator doorAnim;

    bool doorHacked = false;
    float timeBeforeClose = 0.25f;

    [SerializeField] bool restrictOpening = false;

    public class Neuron
    {
        public Collider2D neuronColl;
        public Animator neuronAnim;
        public bool hacked = false;
    }


    void Start()
    {
        doorColl = GetComponent<Collider2D>();
        doorAnim = GetComponent<Animator>();
        playerFilter = new ContactFilter2D();
        playerFilter.SetLayerMask(playerMask);

        neurons = new Neuron[neuronObjects.Length];
        int i = 0;
        foreach (GameObject neuron in neuronObjects)
        {
            Neuron newNeuron = new Neuron();
            newNeuron.neuronColl = neuron.GetComponent<Collider2D>();
            newNeuron.neuronAnim = neuron.GetComponent<Animator>();
            neurons[i++] = newNeuron;
        }
    }

    void Update()
    {
        bool allNeuronsHacked = true;

        foreach(Neuron neu in neurons)
        {
            if(neu.neuronColl.OverlapCollider(playerFilter, new List<Collider2D>()) > 0)
            {
                if(!neu.hacked)
                {
                    neu.hacked = true;
                    SoundController.Instance.PlayNeuronOff();
                    neu.neuronAnim.SetBool("On", false);
                }
            }
            else
            {
                if(neu.hacked)
                {
                    neu.hacked = false;
                    neu.neuronAnim.SetBool("On", true);
                }
                allNeuronsHacked = false;
            }
        }

        if(allNeuronsHacked && !doorHacked)
        {
            doorHacked = true;
            if (!restrictOpening)
            {
                doorAnim.SetBool("open", true);
                doorColl.enabled = false;
            }
        }
        else if (!allNeuronsHacked && doorHacked)
        {
            doorHacked = false;
            if (!restrictOpening)
            {
                StartCoroutine(DoCloseSequence());
            }
        }
    }

    public bool IsHacked()
    {
        return doorHacked;
    }

    IEnumerator DoCloseSequence()
    {
        float timer = 0;
        while (timer < timeBeforeClose)
        {
            timer += Time.deltaTime;

            if (doorHacked)
                yield break;
            
            yield return null;
        }

        doorAnim.SetBool("open", false);
        doorColl.enabled = true;
    }
}

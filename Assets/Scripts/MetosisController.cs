﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class MetosisController : MonoBehaviour
{
    //SINGLETON STUFF
    protected MetosisController() { }
    private static MetosisController _instance = null;

    public static MetosisController Instance
    {
        get
        {
            return MetosisController._instance;
        }
    }

    private void Awake()
    {
        if (_instance != this && _instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;

        DontDestroyOnLoad(this.gameObject);

        if(!debug_mode)
        {
            gameStarted = false;
            allowControl = false;
        }
        else
        {
            gameStarted = true;
            allowControl = true;
            availableCloneIDs = new List<string>() { "B", "C", "D" };
            numPlayers = 1;
        }

        ResetDialogueTriggers();
    }

    //METOSIS STUFF
    public bool allowControl = true;
    bool gameStarted = false;
    bool waitingForNextLevel = false;
    public bool debug_mode = false;

    List<string> availableCloneIDs;

    //UI stuff
    [SerializeField] Image musicButton;
    [SerializeField] Image soundButton;
    [SerializeField] Sprite[] musicIcons;
    [SerializeField] Sprite[] soundIcons;
    private bool sfxOn = true;
    private bool musicOn = true;

    private int numPlayers;
    bool waitingForRestart = false;

    private void Update()
    {
        //Cheat
        if (Input.GetKey(KeyCode.J) && Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.N) && Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.H))
            PlayerPrefs.SetInt("maxUnlockedLevel", 15);

        if (Input.GetButtonDown("RestartKeyboard") || ((Input.GetButton("RestartA") && Input.GetButtonDown("RestartB")) || (Input.GetButtonDown("RestartA") && Input.GetButton("RestartB"))))
            RestartLevel();
    }

    public void RestartLevel()
    {

        PlayerController livingPlayer = GetLivingPlayer();

        SoundController.Instance.PlayGunHit();

        while(livingPlayer != null)
        {
            livingPlayer.TriggerDeathForRestart();
            livingPlayer = GetLivingPlayer();
        }
    }

    public void ToggleSFX()
    {
        sfxOn = !sfxOn;
        soundButton.sprite = sfxOn ? soundIcons[0] : soundIcons[1];
        SoundController.Instance.ToggleSfx(sfxOn);
    }

    public void ToggleMusic()
    {
        musicOn = !musicOn;
        musicButton.sprite = musicOn ? musicIcons[0] : musicIcons[1];
        SoundController.Instance.ToggleMusic(musicOn);
    }

    void PrepThenLoadLevel(int sceneNumber, bool doTween = true)
    {
        availableCloneIDs = new List<string>() { "B", "C", "D" };
        numPlayers = 1;
        if (doTween)
        {
            StartCoroutine(TweenToNewScene(sceneNumber));
        }
        else
        {
            //For title screen, L15, and restart
            allowControl = true;
            SceneManager.LoadScene(sceneNumber);
            DOTween.KillAll();
            Camera.main.transform.position = new Vector3(0, 0, -10);
            Camera.main.orthographicSize = 5;
        }
        SoundController.Instance.ToGameplayMusic();
    }

    IEnumerator TweenToNewScene(int newSceneIndex)
    {

        //Tween to new level
        Scene currentLevel = SceneManager.GetActiveScene();
        Transform oldLevel = currentLevel.GetRootGameObjects()[0].transform;
        oldLevel.transform.position = new Vector3(-18, 0, 0);
        transform.position = new Vector3(-18, 0, 0);

        yield return new WaitForEndOfFrame(); //Without the frame wait there can be some spooky collisions between objects in the old and new level
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame(); 
        yield return new WaitForEndOfFrame();

        SceneManager.LoadScene(newSceneIndex, LoadSceneMode.Additive);

        yield return new WaitForEndOfFrame();

        float tweenTime = 3f;
        transform.DOMoveX(0, tweenTime);
        yield return new WaitForSeconds(tweenTime);

        SceneManager.UnloadSceneAsync(currentLevel);
        yield return new WaitForEndOfFrame();
        waitingForNextLevel = false;
        allowControl = true;

        if (newSceneIndex < 16 && newSceneIndex > PlayerPrefs.GetInt("maxUnlockedLevel", 0))
            PlayerPrefs.SetInt("maxUnlockedLevel", newSceneIndex);
    }

    IEnumerator HoldButtonThenChangeScene(KeyCode button, int newScene)
    {
        float timer = 0;
        while(timer < 1f)
        {
            timer += Time.deltaTime;
            if (!Input.GetKey(button))
                yield break;
            yield return null;
        }

        if(SceneManager.GetActiveScene().buildIndex != 0 && newScene == 0) //full restart
        {
            gameStarted = false;
            allowControl = false;
            waitingForRestart = true;
        }

        bool tweenToNextLevel = true;
        //Debug for quit restart
        if (button == KeyCode.Q)
        {
            ResetDialogueTriggers();
            tweenToNextLevel = false;
        }
        PrepThenLoadLevel(newScene, tweenToNextLevel);
    }

    IEnumerator WaitThenRestart()
    {
        SoundController.Instance.PlayDeathClip();
        yield return new WaitForSeconds(0.75f);
        PrepThenLoadLevel(SceneManager.GetActiveScene().buildIndex, false);
    }

    public string RegisterClone()
    {
        numPlayers++;

        string assignedID = availableCloneIDs[0];
        availableCloneIDs.RemoveAt(0);
        return assignedID;
    }

    //Returns living player with least energy
    public PlayerController GetLivingPlayer()
    {
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        PlayerController leastEnerPlayer = null;
        int leastEner = 999;
        foreach(PlayerController candidate in players)
        {
            if (!candidate.dead)
            {
                if(candidate.GetMitosisEnergy() < leastEner)
                {
                    leastEnerPlayer = candidate;
                    leastEner = candidate.GetMitosisEnergy();
                }
            }
        }
        return leastEnerPlayer;
    }

    void ResetDialogueTriggers()
    {
        PlayerPrefs.SetString("dialogueTriggers", "");
    }

    public void SetDialogueTriggered(int sceneIndex)
    {
        PlayerPrefs.SetString("dialogueTriggers", PlayerPrefs.GetString("dialogueTriggers", "") + "," + sceneIndex);
    }

    public bool WasDialogueTriggered()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (waitingForNextLevel)
            sceneIndex += 1;

        string triggerString = PlayerPrefs.GetString("dialogueTriggers", "");
        string[] triggers = triggerString.Split(new char[]{ ','}, System.StringSplitOptions.RemoveEmptyEntries);

        bool foundTrigger = false;
        for (int i = 0; i < triggers.Length; i++)
        {
            if (int.Parse(triggers[i]) == sceneIndex)
            {
                foundTrigger = true;
                break;
            }
        }

        return foundTrigger;
    }

    public void RegisterPlayerDeath(string cloneID)
    {
        if (waitingForNextLevel)
            return;

        availableCloneIDs.Add(cloneID);

        numPlayers--;
        if (numPlayers == 0)
        {
            StartCoroutine(WaitThenRestart());
        }
    }

    public void ExitReached()
    {
        if (!waitingForNextLevel)
        {
            waitingForNextLevel = true;
            allowControl = false;
            StartCoroutine(WaitThenNextLevel());
        }
    }

    public void TitleToGame(int targetLevel)
    {
        PrepThenLoadLevel(targetLevel, false);
        waitingForNextLevel = false;
    }

    IEnumerator WaitThenNextLevel()
    {
        yield return new WaitForSeconds(1.5f);
        PrepThenLoadLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public int GetNumPlayers()
    {
        return numPlayers;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuronController : MonoBehaviour
{

    /* BIG FAT WARNING! This is deprecated - functionality has all moved to DoorController. 
     * I'm just keeping this around so I don't have to update legacy levels to keep them 
     * from breaking
     * 
     * 
     */

    [SerializeField] GameObject doorToOpen;
    [SerializeField] LayerMask playerMask;

    Collider2D coll;
    Collider2D doorColl;
    ContactFilter2D playerFilter;
    Animator anim;
    Animator doorAnim;

    bool hacked = false;

    float timeBeforeClose = 0.25f;

    void Start()
    {
        coll = GetComponent<Collider2D>();
        playerFilter = new ContactFilter2D();
        playerFilter.SetLayerMask(playerMask);
        anim = GetComponent<Animator>();
        doorColl = doorToOpen.GetComponent<Collider2D>();
        doorAnim = doorToOpen.GetComponent<Animator>();
    }

    void Update()
    {

        if(coll.OverlapCollider(playerFilter, new List<Collider2D>()) > 0)
        {
            if (!hacked)
            {
                hacked = true;
                SoundController.Instance.PlayNeuronOff();
                anim.SetBool("On", false);
                doorAnim.SetTrigger("open");
                doorColl.enabled = false;
            }
        }
        else
        {
            if(hacked)
            {
                hacked = false;
                anim.SetBool("On", true);
                StartCoroutine(DoCloseSequence());
            }
        }
    }

    IEnumerator DoCloseSequence()
    {
        float timer = 0;
        while(timer < timeBeforeClose)
        {
            timer += Time.deltaTime;

            if (!hacked)
                yield return 0;

            yield return null;
        }

        doorAnim.SetTrigger("close");
        doorColl.enabled = true;
    }
}

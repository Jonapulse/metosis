﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    bool on = false;
    [SerializeField] float period;

    Animator lightAnim;
    void Start()
    {
        lightAnim = GetComponent<Animator>();
        StartCoroutine(ToggleLightRoutine());
    }

    IEnumerator ToggleLightRoutine()
    {
        while(true && lightAnim.gameObject)
        {
            yield return new WaitForSeconds(period);

            on = !on;
            lightAnim.SetBool("on", on);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphincterPulse : MonoBehaviour
{
    [SerializeField] float period = 15f;
    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(PulseSphincter());
    }

    IEnumerator PulseSphincter()
    {
        while (true && anim.gameObject)
        {
            yield return new WaitForSeconds(period);
            anim.SetTrigger("pulse");
        }
    }
}

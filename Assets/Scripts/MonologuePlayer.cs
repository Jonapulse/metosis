﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class MonologuePlayer : MonoBehaviour
{
    AudioSource audio;
    [SerializeField] AudioClip monologue;
    [SerializeField] AudioClip music;
    [SerializeField] TextMeshPro monologueText;
    [SerializeField] TextMeshPro[] fadeInTexts;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        StartCoroutine(PlayMonologue());

        SoundController.Instance.StopMusic();
    }
    IEnumerator PlayMonologue()
    {
        monologueText.DOFade(1, 2f);

        yield return new WaitForSeconds(3f);

        audio.clip = monologue;
        audio.Play();

        yield return new WaitForSeconds(monologue.length - 8f);

        monologueText.DOFade(0, 3f);

        yield return new WaitForSeconds(8f);

        foreach (TextMeshPro nexText in fadeInTexts)
            nexText.DOFade(1, 2f);

        yield return new WaitForSeconds(2f);

        audio.clip = music;
        audio.Play();

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class CreditsFader : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI endText;
    [SerializeField] TextMeshProUGUI[] creditsTexts;

    void Start()
    {
        Color transparentRed = new Color(endText.color.r, endText.color.g, endText.color.b, 0);
        endText.color = transparentRed;
        foreach (TextMeshProUGUI t in creditsTexts)
            t.color = transparentRed;

        StartCoroutine(RunCredits());
    }

    IEnumerator RunCredits()
    {
        endText.DOFade(1, 2f);

        yield return new WaitForSeconds(3f);

        foreach (TextMeshProUGUI t in creditsTexts)
            t.DOFade(1, 2f);

        yield return new WaitForSeconds(3f);
    }
}

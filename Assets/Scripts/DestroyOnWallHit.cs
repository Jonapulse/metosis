﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnWallHit : MonoBehaviour
{
    [SerializeField] LayerMask floorLayer;
    Rigidbody2D body;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Debug.DrawRay(transform.position, body.velocity * 1/60, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, body.velocity.normalized, Mathf.Abs(body.velocity.x) * 1/60, floorLayer);
        if (hit)
        {
            SoundController.Instance.PlayRicochet();
            Destroy(this.gameObject);
        }
    }
}

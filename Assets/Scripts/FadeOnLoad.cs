﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeOnLoad : MonoBehaviour
{
    [SerializeField] bool fadeOut;
    [SerializeField] bool andThenLoadNextLevel = false;
    Image fade;

    void Start()
    {
        fade = GetComponent<Image>();
        fade.color = Color.black;
        fade.DOFade(0, 2f).SetEase(Ease.InQuad);
    }
}
